package ru.nlmk.study.jse26.nio2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

public class Main {
    public static void main(String[] args) throws IOException {
        File currentDirFile = new File(".");
        String strPath = "";
        Path path = null;
        try {
            path = Paths.get(currentDirFile.getCanonicalPath());
            strPath = path.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
/*        System.out.println(strPath);

        System.out.println("hasFile:" + Files.notExists(path));
        System.out.println("file? " + Files.isRegularFile(path));*/

        //String fileName = "myfile__" + UUID.randomUUID().toString() + ".txt";
/*        String fileName = "myfile__.txt";
        Path p = Paths.get(strPath + "/" + fileName);
        System.out.println(Files.exists(p));
        Files.createFile(p);
        System.out.println(Files.exists(p));*/

/*        String dirName = "моядиректория__" + UUID.randomUUID().toString();
        Path p2 = Paths.get(strPath + "/" + dirName + "/" + dirName);
        System.out.println(Files.exists(p2));
        Files.createDirectories(p2);
        System.out.println(Files.exists(p2));
        System.out.println(Files.isRegularFile(p2));
        System.out.println(Files.isDirectory(p2));*/

        Path dir1 = Paths.get(strPath + "/firstdir__" + UUID.randomUUID().toString());
        Path dir2 = Paths.get(strPath + "/otherdir__" + UUID.randomUUID().toString());
        Files.createDirectory(dir1);
        Files.createDirectory(dir2);
        Path file1 = dir1.resolve("filetocopy.txt");
        Path file2 = dir2.resolve("filetocopy.txt");
        Files.createFile(file1);
        Files.createFile(file2);
        System.out.println(Files.exists(file1));
        System.out.println(Files.exists(file2));
        //Files.copy(file1, file2);
        Files.copy(file1, file2, StandardCopyOption.REPLACE_EXISTING);
    }
}
