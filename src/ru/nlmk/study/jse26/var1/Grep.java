package ru.nlmk.study.jse26.var1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Grep {
    private static Pattern linePattern = Pattern.compile(".*\r?\n");
    private static Pattern pattern;
    private static Charset charset = StandardCharsets.UTF_8;
    private static CharsetDecoder decoder = charset.newDecoder();

    private static void compile(String inpPattern){
        pattern = Pattern.compile(inpPattern);
    }

    public static void main(String[] args) {
        String fileName = "where.txt";
        String pattern = "Lorem";
        compile(pattern);

        File file = new File(fileName);
        try {
            grep(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void grep(File file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        FileChannel fileChannel = fileInputStream.getChannel();
        int fileChannelSize = (int)fileChannel.size();
        MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannelSize);
        CharBuffer charBuffer = decoder.decode(mappedByteBuffer);
        grep (file, charBuffer);
        fileChannel.close();
    }

    private static void grep(File file, CharBuffer charBuffer) {
        Matcher linePatternMatcher = linePattern.matcher(charBuffer);
        Matcher patternMatcher = null;
        int lines = 0;
        while (linePatternMatcher.find()){
            lines++;
            CharSequence charSequence = linePatternMatcher.group();
            if(patternMatcher == null){
                patternMatcher = pattern.matcher(charSequence);
            } else {
                patternMatcher.reset(charSequence);
            }
            if (patternMatcher.find()){
                System.out.println(file + ":" + lines + ":" + charSequence);
            }
            if (linePatternMatcher.end() == charBuffer.limit()){
                break;
            }
        }
    }
}
