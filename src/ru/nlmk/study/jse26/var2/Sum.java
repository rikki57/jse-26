package ru.nlmk.study.jse26.var2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;


public class Sum {
    private static int sum(ByteBuffer byteBuffer) {
        int sum = 0;
        while (byteBuffer.hasRemaining()) {
            if ((sum & 1) != 0)
                sum = (sum >> 1) + 0x8000;
            else
                sum >>= 1;
            sum += byteBuffer.get() & 0xff;
            sum &= 0xffff;
        }
        return sum;
    }

    private static void sum(File file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        FileChannel fileChannel = fileInputStream.getChannel();
        int size = (int) fileChannel.size();
        MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        int sum = sum(mappedByteBuffer);
        System.out.println(file + " : " + sum);
    }

    public static void main(String[] args) {
        File file = new File("where.txt");
        try {
            sum(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
