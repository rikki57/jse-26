package ru.nlmk.study.jse26.var3;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class HelloServer {
    private static int port;

    private static Charset charset = StandardCharsets.US_ASCII;
    private static CharsetEncoder encoder = charset.newEncoder();

    private static ServerSocketChannel setup() throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddress.getLocalHost(), port);
        System.out.println(InetAddress.getLocalHost());
        serverSocketChannel.socket().bind(inetSocketAddress);
        return serverSocketChannel;
    }

    private static void serve(ServerSocketChannel serverSocketChannel) throws IOException {
        SocketChannel socketChannel = serverSocketChannel.accept();
        String now = new Date().toString();
        System.out.println("Accepted connection" + now);
        socketChannel.write(encoder.encode(CharBuffer.wrap("Hello! " + now + "\r\n")));
        socketChannel.close();
    }

    public static void main(String[] args) {
        port = 22023;
        try {
            ServerSocketChannel serverSocketChannel = setup();
            for (;;){
                serve(serverSocketChannel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
