package ru.nlmk.study.jse26.var4;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class HelloClient {
    private static int port;

    private static Charset charset = Charset.forName("US-ASCII");
    private static CharsetDecoder decoder = charset.newDecoder();
    private static ByteBuffer byteBuffer = ByteBuffer.allocateDirect(5);

    private static void sendQuery(String host) throws IOException {
        InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddress.getByName(host), port);
        SocketChannel socketChannel = null;
        socketChannel = SocketChannel.open();
        socketChannel.connect(inetSocketAddress);
        byteBuffer.clear();
        socketChannel.read(byteBuffer);
        byteBuffer.flip();
        CharBuffer cb = decoder.decode(byteBuffer);
        System.out.println(cb);
    }

    public static void main(String[] args) {
        port = 22023;
        String host = "192.168.8.100";
        try {
            sendQuery(host);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
